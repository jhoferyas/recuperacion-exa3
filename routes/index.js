var express = require('express');
var router = express.Router();
//autor
var cliente= require('../controllers/clienteController');
var clienteController = new cliente();
//libro
var factura= require('../controllers/facturaController');
var facturaController = new factura();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', {
      titulo: 'Express',
      fragmento: 'fragmentos/frm_principal'
  });
});
router.get('/cliente', clienteController.verCliente);
router.post('/cliente/guardar', clienteController.guardarCliente);
router.get('/lista_cliente', facturaController.obtenerClientes);
router.get('/factura', facturaController.verFactura);
router.get('/lista_factura', facturaController.obtenerFacturas);
router.post('/factura/guardar', facturaController.guardarFactura);
router.get('/factura_buscar/:buscar', facturaController.buscarFActura);
router.get('/factura_cliente/:external', facturaController.obtenerGastos);
router.get('/reporte/:external', facturaController.generarReporteCliente);
router.get('/reporte', facturaController.generarReporte);
router.get('/clasificacion', facturaController.obtenerClasificacion);

module.exports = router;
